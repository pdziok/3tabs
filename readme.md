# 3tabs
## Overview
A simple project for aggregating from different sources. It handles text files, rss and json-feed.

## Requirements
* php > 5.5
* composer

## Installation

1. Clone repository
2. Run `composer install`
3. Configure your vhost. App is not host dependent.

## Configuration

All necessary configuration could be found in `config/config.yml`. For now on application is configured.
Any other sources might be appended there, acording to schema.