<?php

define('ROOT_PATH', realpath(__DIR__ . '/..'));
chdir(ROOT_PATH);

require_once 'vendor/autoload.php';

try {
    $app = require_once 'src/bootstrap.php';
    require_once 'src/routes.php';

    $app->run();
} catch (\Exception $e) {
    echo (string)$e;
}
