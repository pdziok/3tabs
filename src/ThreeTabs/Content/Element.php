<?php

namespace ThreeTabs\Content;

class Element
{
    /** @var string */
    public $name;
    /** @var  array */
    public $data;
    /** @var string */
    public $partial;

    public function __construct($name, $data, $partial)
    {
        $this->name    = $name;
        $this->data    = $data;
        $this->partial = $partial;
    }
}