<?php


namespace ThreeTabs\Content;

class Container implements \IteratorAggregate
{

    /** @var Element[] */
    private $elements;

    public function __construct(array $elements = [])
    {
        $this->setElements($elements);
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->elements);
    }

    /**
     * @param Element[] $elements
     */
    public function setElements(array $elements)
    {
        $this->elements = [];
        foreach ($elements as $element) {
            $this->addElement($element);
        }
    }

    public function addElement(Element $element)
    {
        $this->elements[] = $element;
    }
}