<?php

namespace ThreeTabs\Content;

use Assert\Assertion;

class ContainerBuilder
{

    /** @var  ContentFactory */
    private $contentFactory;

    public function __construct(ContentFactory $contentFactory)
    {
        $this->contentFactory = $contentFactory;
    }

    public function build($config)
    {
        Assertion::notEmpty($config);
        $container = new Container();

        foreach ($config as $elementConfig) {
            $container->addElement($this->contentFactory->create($elementConfig));
        }

        return $container;
    }
}