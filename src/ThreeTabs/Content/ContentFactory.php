<?php

namespace ThreeTabs\Content;

use Assert\Assertion;
use ThreeTabs\Reader\ReaderFactory;
use ThreeTabs\Reader\ReaderFactoryException;
use ThreeTabs\Reader\ReaderInterface;
use ThreeTabs\Traverse\TraversalFactory;
use ThreeTabs\Traverse\TraversalFactoryException;
use ThreeTabs\Traverse\TraversalInterface;

class ContentFactory
{

    /** @var ReaderFactory */
    private $readerFactory;

    /** @var TraversalFactory */
    private $traversalFactory;

    public function __construct(ReaderFactory $readerFactory, TraversalFactory $traversalFactory)
    {
        $this->readerFactory    = $readerFactory;
        $this->traversalFactory = $traversalFactory;
    }

    public function create($config)
    {
        Assertion::keyExists($config, 'name', 'Content element name is not specified');
        Assertion::keyExists($config, 'partial', 'Content element partial is not specified');
        Assertion::keyExists($config, 'sections', 'Content sections are not specified');
        Assertion::isArray($config, 'sections', 'Content sections are not valid');

        $reader = $this->createReader($config);
        $input  = $reader->read($config['reader']['url']);

        $data = [];
        foreach ($config['sections'] as $sectionConfig) {
            $traversal                    = $this->createTraversal($sectionConfig);
            $data[$sectionConfig['name']] = $traversal->traverse($input);
        }

        $element = new Element($config['name'], $data, $config['partial']);

        return $element;
    }

    /**
     * @param array $config
     *
     * @return ReaderInterface
     */
    private function createReader($config)
    {
        try {
            Assertion::keyExists($config, 'reader', 'Reader config is missing');

            return $this->readerFactory->createFromConfig($config['reader']);
        } catch (\InvalidArgumentException $e) {
            throw new ContentFactoryException(sprintf('Cannot create content', $config['name']), 0, $e);
        } catch (ReaderFactoryException $e) {
            throw new ContentFactoryException(sprintf('Cannot create content', $config['name']), 0, $e);
        }
    }

    /**
     * @param array $config
     *
     * @return TraversalInterface
     */
    private function createTraversal($config)
    {
        try {
            Assertion::keyExists($config, 'traversal', 'Reader config is missing');

            return $this->traversalFactory->createFromConfig($config['traversal']);
        } catch (\InvalidArgumentException $e) {
            throw new ContentFactoryException(sprintf('Cannot create content section'), 0, $e);
        } catch (TraversalFactoryException $e) {
            throw new ContentFactoryException(sprintf('Cannot create content section'), 0, $e);
        }
    }

}