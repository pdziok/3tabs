<?php

namespace ThreeTabs;

use Silex\Application as SilexApplication;
use Silex\Application\TwigTrait;

class Application extends SilexApplication
{
    use TwigTrait;
    use SilexApplication\MonologTrait;
}