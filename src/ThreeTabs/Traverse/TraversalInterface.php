<?php

namespace ThreeTabs\Traverse;

interface TraversalInterface
{

    public function traverse($data);
}