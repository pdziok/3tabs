<?php


namespace ThreeTabs\Traverse;


class Limit implements TraversalInterface
{
    /** @var int */
    private $limit;
    /** @var int */
    private $offset;

    public function __construct($limit, $offset = 0)
    {
        $this->limit  = $limit;
        $this->offset = $offset;
    }

    public function traverse($data)
    {
        return array_slice($data, $this->offset, $this->limit);
    }
}