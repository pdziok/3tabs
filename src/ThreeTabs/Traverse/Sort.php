<?php


namespace ThreeTabs\Traverse;


use Assert\Assertion;

class Sort implements TraversalInterface
{
    /** @var int */
    private $direction;
    private $field;

    const ASCENDING = 0;
    const DESCENDING = 1;

    public function __construct($direction, $field = null)
    {
        $this->direction = $direction;
        $this->field     = $field;
    }

    public function traverse($data)
    {
        Assertion::isArray($data);
        uasort($data, array($this, 'compare'));

        return $data;
    }

    protected function compare($from, $to)
    {

        if ($this->field) {
            Assertion::keyExists($from, $this->field);
            Assertion::keyExists($to, $this->field);

            $from = $from[$this->field];
            $to   = $to[$this->field];
        }

        if ($from > $to) {
            return $this->direction == self::ASCENDING ? 1 : -1;
        } elseif ($from < $to) {
            return $this->direction == self::ASCENDING ? -1 : 1;
        } else {
            return 0;
        }
    }

}