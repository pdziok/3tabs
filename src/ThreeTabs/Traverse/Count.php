<?php

namespace ThreeTabs\Traverse;

use Assert\Assertion;

class Count implements TraversalInterface
{
    private $groupBy;

    public function __construct($groupBy = null)
    {
        $this->groupBy = $groupBy;
    }

    public function traverse($data)
    {
        if ($this->groupBy) {
            return $this->groupingSumming($data);
        } else {
            return $this->simpleSumming($data);
        }
    }

    private function simpleSumming($data)
    {
        return count($data);
    }

    private function groupingSumming($data)
    {
        $groupBy = $this->groupBy;

        $result = [];
        foreach ($data as $row) {
            Assertion::keyExists($row, $groupBy);

            $groupingValue = $row[$groupBy];
            if (!array_key_exists($groupingValue, $result)) {
                $result[$groupingValue] = 0;
            }

            $result[$groupingValue]++;
        }

        return $result;
    }
}