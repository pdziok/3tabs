<?php

namespace ThreeTabs\Traverse;

use Assert\Assertion;

class Sum implements TraversalInterface
{
    private $field;
    private $groupBy;

    public function __construct($field, $groupBy = null)
    {
        $this->field   = $field;
        $this->groupBy = $groupBy;
    }

    public function traverse($data)
    {
        if ($this->groupBy) {
            return $this->groupingSumming($data);
        } else {
            return $this->simpleSumming($data);
        }
    }

    private function simpleSumming($data)
    {
        $field = $this->field;

        $result = 0;
        foreach ($data as $row) {
            Assertion::keyExists($row, $field);

            $result += $row[$field];
        }

        return $result;
    }

    private function groupingSumming($data)
    {
        $field   = $this->field;
        $groupBy = $this->groupBy;

        $result = [];
        foreach ($data as $row) {
            Assertion::keyExists($row, $field);
            Assertion::keyExists($row, $groupBy);

            $groupingValue = $row[$groupBy];
            if (!array_key_exists($groupingValue, $result)) {
                $result[$groupingValue] = 0;
            }

            $result[$groupingValue] += $row[$field];
        }

        return $result;
    }
}