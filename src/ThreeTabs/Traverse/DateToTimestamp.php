<?php

namespace ThreeTabs\Traverse;

use Assert\Assertion;

class DateToTimestamp implements TraversalInterface
{
    private $map = [
        'januar'    => 'January',
        'februar'   => 'February',
        'mars'      => 'March',
        'april'     => 'April',
        'mai'       => 'May',
        'juni'      => 'June',
        'juli'      => 'July',
        'august'    => 'August',
        'september' => 'September',
        'oktober'   => 'October',
        'november'  => 'November',
        'desember'  => 'December',
    ];

    private $field;

    public function __construct($field)
    {
        $this->field = $field;
    }

    public function traverse($data)
    {
        $output = [];
        foreach ($data as $row) {
            $outputRow = $row;

            Assertion::keyExists($outputRow, $this->field);
            $outputRow[$this->field] = $this->translate($outputRow[$this->field]);

            $output[] = $outputRow;
        }

        return $output;
    }


    /**
     * @param $phrase
     *
     * @return mixed|string
     */
    private function translate($phrase)
    {
        $phrase  = strtolower($phrase);
        $search  = array_keys($this->map);
        $replace = array_values($this->map);
        $phrase  = str_replace($search, $replace, $phrase);
        $time    = strtotime($phrase);

        return $time;
    }
}