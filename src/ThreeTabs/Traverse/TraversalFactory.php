<?php

namespace ThreeTabs\Traverse;

use Assert\Assertion;

class TraversalFactory
{

    /** @var string[] */
    private $availableTraversals;

    public function __construct(array $availableTraversals = [])
    {
        $this->availableTraversals = $availableTraversals;
    }

    /**
     * @param string $type
     * @param array  $arguments
     * @param array  $chains
     *
     * @return TraversalInterface
     */
    public function create($type, $arguments = [], $chains = [])
    {
        try {
            Assertion::string($type, 'Traversal type must be a string');
            Assertion::keyExists($this->availableTraversals, $type, 'Traversal "%s" is not configured');

            $class = $this->availableTraversals[$type];
            Assertion::classExists($class, 'Traversal does not exists');

            $traversal = $this->instantiate($class, $arguments);

            if ($traversal instanceof TraversalChain) {
                $this->configureTraversalChain($traversal, $chains);
            }

        } catch (\InvalidArgumentException $e) {
            throw new TraversalFactoryException('Cannot create traversal', 0, $e);
        }

        return $traversal;
    }

    public function createFromConfig($config)
    {
        Assertion::keyExists($config, 'type', 'Traversal type is not specified');
        $type      = $config['type'];
        $arguments = !empty($config['arguments']) ? $config['arguments'] : [];
        $chains    = !empty($config['chain']) ? $config['chain'] : [];

        return $this->create($type, $arguments, $chains);
    }

    /**
     * @param $class
     *
     * @param $arguments
     *
     * @return object
     */
    protected function instantiate($class, $arguments)
    {
        $reflection = new \ReflectionClass($class);
        $arguments  = (array)$arguments;
        $reader     = $reflection->newInstanceArgs($arguments);

        return $reader;
    }

    protected function configureTraversalChain(TraversalChain $traversal, $chains)
    {
        foreach ($chains as $chainElement) {

            Assertion::keyExists($chainElement, 'type');
            $type      = $chainElement['type'];
            $arguments = !empty($chainElement['arguments']) ? $chainElement['arguments'] : [];
            $traversal->addTraversal($this->create($type, $arguments));
        }
    }
}