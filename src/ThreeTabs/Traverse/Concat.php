<?php

namespace ThreeTabs\Traverse;

use Assert\Assertion;

class Concat implements TraversalInterface
{
    private $fields;
    private $with;
    private $to;

    public function __construct(array $fields, $with, $to)
    {
        $this->fields = $fields;
        $this->with   = $with;
        $this->to     = $to;
    }

    public function traverse($data)
    {
        $output = [];
        foreach ($data as $row) {
            $outputRow = $row;

            $valuesToBeConcatenated = $this->collectValuesToConcat($outputRow);
            $outputRow[$this->to]   = implode($this->with, $valuesToBeConcatenated);

            $output[] = $outputRow;
        }

        return $output;
    }

    /**
     * @param $outputRow
     *
     * @return array
     */
    private function collectValuesToConcat($outputRow)
    {
        $valuesToBeConcatenated = [];
        foreach ($this->fields as $field) {
            Assertion::keyExists($outputRow, $field, sprintf('Cannot find field "%s"', $field));

            $valuesToBeConcatenated[] = $outputRow[$field];
        }

        return $valuesToBeConcatenated;
    }
}