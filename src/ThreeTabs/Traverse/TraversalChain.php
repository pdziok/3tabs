<?php


namespace ThreeTabs\Traverse;


class TraversalChain implements TraversalInterface
{

    /** @var TraversalInterface[] */
    private $traversals = [];

    public function __construct(array $traversals = [])
    {
        $this->setTraversals($traversals);
    }

    public function traverse($data)
    {
        foreach ($this->traversals as $traversal) {
            $data = $traversal->traverse($data);
        }

        return $data;
    }

    /**
     * @param TraversalInterface[] $traversals
     *
     * @return $this
     */
    public function setTraversals(array $traversals)
    {
        $this->traversals = [];
        $this->addTraversals($traversals);

    }

    /**
     * @param TraversalInterface[] $traversals
     */
    public function addTraversals(array $traversals)
    {
        foreach ($traversals as $traversal) {
            $this->addTraversal($traversal);
        }
    }

    /**
     * @param TraversalInterface $traversal
     */
    public function addTraversal(TraversalInterface $traversal)
    {
        $this->traversals[] = $traversal;
    }
}