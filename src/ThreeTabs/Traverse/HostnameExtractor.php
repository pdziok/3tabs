<?php


namespace ThreeTabs\Traverse;


use Assert\Assertion;

class HostnameExtractor implements TraversalInterface
{
    private $field;

    public function __construct($field)
    {
        $this->field = $field;
    }

    public function traverse($data)
    {
        $result = [];
        foreach ($data as $row) {
            Assertion::keyExists($row, $this->field);
            Assertion::url($row[$this->field]);
            $rowCopy               = $row;
            $rowCopy[$this->field] = parse_url($rowCopy[$this->field], PHP_URL_HOST);
            $result[]              = $rowCopy;
        }

        return $result;
    }
}