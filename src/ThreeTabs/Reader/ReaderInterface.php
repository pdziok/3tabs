<?php

namespace ThreeTabs\Reader;

interface ReaderInterface
{

    public function read($url);
}