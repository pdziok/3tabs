<?php

namespace ThreeTabs\Reader;

use Assert\Assertion;

class File implements ReaderInterface
{
    /** @var string */
    private $format;

    public function __construct($format = null)
    {
        $this->format = $format;
    }

    public function read($url)
    {
        Assertion::file($url);
        $fileContent = file_get_contents($url);
        $fileContent = explode("\n", $fileContent);
        $fileContent = array_map('trim', $fileContent);
        $fileContent = array_filter($fileContent);

        $output = [];
        foreach ($fileContent as $line) {
            $output[] = $this->extract($line);
        }

        return $output;
    }

    private function extract($line)
    {
        if (!$this->format || !preg_match('/' . $this->format . '/', $line, $matches)) {
            return $line;
        }

        $output = [];
        foreach (array_filter(array_keys($matches), 'is_string') as $match) {
            $output[$match] = $matches[$match];
        }

        return $output;
    }
}