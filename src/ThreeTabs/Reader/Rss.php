<?php

namespace ThreeTabs\Reader;

class Rss implements ReaderInterface
{
    public function read($url)
    {
        $content = file_get_contents($url);
        $data    = array_filter(
            json_decode(
                json_encode(simplexml_load_string($content)),
                true
            ),
            function ($value) {
                if (is_string($value)) {
                    $value = trim($value);
                }

                return !empty($value);
            }
        );

        return $data['channel']['item'];
    }
}