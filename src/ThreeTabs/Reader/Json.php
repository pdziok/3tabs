<?php

namespace ThreeTabs\Reader;

class Json implements ReaderInterface
{
    public function read($url)
    {
        $fileContent = file_get_contents($url);

        return json_decode($fileContent, true);
    }
}