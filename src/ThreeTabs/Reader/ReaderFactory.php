<?php

namespace ThreeTabs\Reader;

use Assert\Assertion;

class ReaderFactory
{

    /** @var string[] */
    private $availableReaders;

    public function __construct(array $availableReaders = [])
    {
        $this->availableReaders = $availableReaders;
    }

    /**
     * @param array $config
     *
     * @return ReaderInterface
     */
    public function create($type, $arguments = [])
    {
        try {
            Assertion::string($type, 'Reader type must be a string');
            Assertion::keyExists($this->availableReaders, $type, 'Reader "%s" is not configured');

            $class = $this->availableReaders[$type];
            Assertion::classExists($class, 'Reader does not exists');

            $reader = $this->instantiate($arguments, $class);

        } catch (\InvalidArgumentException $e) {
            throw new ReaderFactoryException('Cannot create reader', 0, $e);
        }

        return $reader;
    }

    public function createFromConfig($config)
    {
        Assertion::keyExists($config, 'type', 'Reader type is not specified');
        $type      = $config['type'];
        $arguments = !empty($config['arguments']) ? $config['arguments'] : [];

        return $this->create($type, $arguments);
    }

    /**
     * @param $arguments
     * @param $class
     *
     * @return object
     */
    protected function instantiate($arguments, $class)
    {
        $reflection = new \ReflectionClass($class);
        $arguments  = (array)$arguments;
        $reader     = $reflection->newInstanceArgs($arguments);

        return $reader;
    }
}