<?php

$app->get('/', function() use ($app) {
    $container = $app['container.builder']->build($app['config']['content-provider']);

    return $app->render(
        'index.twig',
        [
            'content' => $container
        ]
    );
});