<?php

$app = new ThreeTabs\Application();

$app['debug'] = true;
$app['root.path'] = ROOT_PATH;
$app->register(new \DerAlex\Silex\YamlConfigServiceProvider($app['root.path'] . '/config/config.yml'));
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => $app['root.path'] . '/views',
));
$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => $app['root.path'] . '/var/log/development.log',
));

$app['data.reader.factory'] = $app->share(
    function () {
        return new \ThreeTabs\Reader\ReaderFactory(
            [
                'file' => 'ThreeTabs\\Reader\\File',
                'rss'  => 'ThreeTabs\\Reader\\Rss',
                'json' => 'ThreeTabs\\Reader\\Json',
            ]
        );
});

$app['data.traversal.factory'] = $app->share(
    function () {
        return new \ThreeTabs\Traverse\TraversalFactory(
            [
                'chain'             => 'ThreeTabs\Traverse\TraversalChain',
                'hostnameExtractor' => 'ThreeTabs\Traverse\HostnameExtractor',
                'sum'               => 'ThreeTabs\Traverse\Sum',
                'sort'              => 'ThreeTabs\Traverse\Sort',
                'limit'             => 'ThreeTabs\Traverse\Limit',
                'count'             => 'ThreeTabs\Traverse\Count',
                'concat'            => 'ThreeTabs\Traverse\Concat',
                'dateToTimestamp'   => 'ThreeTabs\Traverse\DateToTimestamp',
            ]
        );
    }
);

$app['content.factory']   = $app->share(
    function ($app) {
        return new \ThreeTabs\Content\ContentFactory($app['data.reader.factory'], $app['data.traversal.factory']);
    }
);
$app['container.builder'] = $app->share(
    function ($app) {
        return new \ThreeTabs\Content\ContainerBuilder($app['content.factory']);
});

return $app;