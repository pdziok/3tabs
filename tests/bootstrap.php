<?php
ini_set('error_reporting', E_ALL | E_STRICT);
define('TEST_PATH', __DIR__);
define('ROOT_PATH', TEST_PATH . '/..');

$files = array(__DIR__ . '/../vendor/autoload.php');

foreach ($files as $file) {
    if (file_exists($file)) {
        $loader = require $file;
        break;
    }
}

if (!isset($loader)) {
    throw new RuntimeException('vendor/autoload.php could not be found. Did you run `php composer.phar install`?');
}

unset($files, $file, $loader);