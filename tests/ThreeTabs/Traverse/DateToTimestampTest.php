<?php


namespace ThreeTabs\Traverse;


class DateToTimestampTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testTraversingWithNonExistentKey()
    {
        $traversal = new DateToTimestamp('timestamp');
        $output    = $traversal->traverse(
            [
                '21 January 2014 14:23:22',
                '12 Match 2014 11:21:30'
            ]
        );

        $this->assertEquals([], $output);
    }

    public function testNorwegianDates()
    {
        $traversal = new DateToTimestamp('timestamp');
        $ourput    = $traversal->traverse(
            [
                ['timestamp' => '21 Januar 2014 14:23:22'],
                ['timestamp' => '12 Mars 2014 11:21:30']
            ]
        );

        $this->assertEquals(
            [
                ['timestamp' => 1390310602],
                ['timestamp' => 1394619690]
            ],
            $ourput
        );
    }
}
