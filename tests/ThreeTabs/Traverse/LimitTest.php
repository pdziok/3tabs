<?php

namespace ThreeTabs\Traverse;

class LimitTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider limitData
     */
    public function testLimit($input, $expected)
    {
        $traversal = new Limit(4);
        $output    = $traversal->traverse($input);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);
    }

    /**
     * @dataProvider limitWithOffsetData
     */
    public function testLimitWithOffset($input, $expected)
    {
        $traversal = new Limit(5, 2);
        $output    = $traversal->traverse($input);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);
    }

    public function limitData()
    {
        $input = [
            4, 56, 12, [1], 'string', 2.4, new \stdClass()
        ];

        $expected = [
            4, 56, 12, [1]
        ];

        return [
            [$input, $expected]
        ];
    }

    public function limitWithOffsetData()
    {
        $input = [
            4, 56, 12, [1], 'string', 2.4, new \stdClass()
        ];

        $expected = [
            12, [1], 'string', 2.4, new \stdClass()
        ];

        return [
            [$input, $expected]
        ];
    }
}
