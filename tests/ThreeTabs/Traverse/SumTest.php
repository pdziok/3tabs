<?php


namespace ThreeTabs\Traverse;


class SumTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSummingByNonExistingField()
    {
        $traversal = new Sum('non existing');
        $traversal->traverse(
            [
                ['some value', 'another value'],
                ['some value', 'another value']
            ]
        );
    }

    /**
     * @dataProvider simpleSumming
     */
    public function testSummingSimple($input, $expected)
    {
        $traversal = new Sum('amount');
        $output    = $traversal->traverse($input);

        $this->assertInternalType('numeric', $output);
        $this->assertEquals($expected, $output);
    }

    /**
     * @dataProvider simpleSumming
     * @expectedException \InvalidArgumentException
     */
    public function testGroupingSummingWithNonExistingGroupingKey($input)
    {
        $traversal = new Sum('amount', 'city');
        $traversal->traverse($input);
    }

    /**
     * @dataProvider groupingSumming
     */
    public function testSummingWithGrouping($input, $expected)
    {
        $traversal = new Sum('amount', 'city');
        $output    = $traversal->traverse($input);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);
    }

    public function simpleSumming()
    {
        $input = [
            [
                'name'   => 'dummy name',
                'amount' => 42
            ],
            [
                'name'   => 'simple name',
                'amount' => 87
            ]
        ];

        $expected = 129;

        return [
            [$input, $expected]
        ];
    }

    public function groupingSumming()
    {
        $input = [
            [
                'name'   => 'dummy name',
                'city'   => 'cracow',
                'amount' => 42
            ],
            [
                'name'   => 'another name',
                'city'   => 'cracow',
                'amount' => 20
            ],
            [
                'name'   => 'simple name',
                'city'   => 'warsaw',
                'amount' => 87
            ]
        ];

        $expected = [
            'cracow' => 62,
            'warsaw' => 87
        ];

        return [
            [$input, $expected]
        ];
    }
}
