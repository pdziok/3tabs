<?php


namespace ThreeTabs\Traverse;


class TraversalFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \ThreeTabs\Traverse\TraversalFactoryException
     */
    public function testCreatingReadingUsingInvalidType()
    {
        $factory = new TraversalFactory();
        $factory->create([]);
    }

    /**
     * @expectedException \ThreeTabs\Traverse\TraversalFactoryException
     */
    public function testCreatingNonConfiguredTraverse()
    {
        $factory = new TraversalFactory();
        $factory->create('mind');
    }

    public function testCreatingTraverseWithArguments()
    {
        $factory = new TraversalFactory(
            [
                'sum' => Sum::class
            ]
        );
        $factory->create(
            'sum',
            [
                'amount'
            ]
        );
    }

    public function testCreatingTraversalChain()
    {
        $factory = new TraversalFactory(
            [
                'chain' => TraversalChain::class,
                'sum'   => Sum::class,
                'sort'  => Sort::class
            ]
        );
        $factory->create(
            'chain',
            [],
            [
                [
                    'type'      => 'sum',
                    'arguments' => ['amount']
                ],
                [
                    'type'      => 'sort',
                    'arguments' => [Sort::DESCENDING, 'amount']
                ]
            ]
        );
    }


    public function testCreatingReaderFromConfig()
    {
        $factory = new TraversalFactory(
            [
                'chain' => TraversalChain::class,
                'sum'   => Sum::class,
                'sort'  => Sort::class
            ]
        );
        $factory->createFromConfig(
            [
                'type'   => 'chain',
                'chains' => [
                    [
                        'type'      => 'sum',
                        'arguments' => ['amount']
                    ],
                    [
                        'type'      => 'sort',
                        'arguments' => [Sort::DESCENDING, 'amount']
                    ]
                ]
            ]
        );
    }


}
