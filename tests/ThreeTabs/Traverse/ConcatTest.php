<?php


namespace ThreeTabs\Traverse;


class ConcatTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testConcatenatingByNonExistingField()
    {
        $traversal = new Concat([0, 2], ' ', 'output');
        $traversal->traverse(
            [
                ['some value', 'another value'],
                ['some value', 'another value']
            ]
        );
    }

    /**
     * @dataProvider simpleConcatenating
     */
    public function testConcatenatingSimple($input, $expected)
    {
        $traversal = new Concat(['fname', 'lname'], ' ', 'full-name');
        $output    = $traversal->traverse($input);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);
    }

    public function simpleConcatenating()
    {
        $input = [
            [
                'fname'  => 'dummy',
                'lname'  => 'name',
                'amount' => 42
            ],
            [
                'fname'  => 'simple',
                'lname'  => 'name',
                'amount' => 87
            ]
        ];

        $expected = [
            [
                'fname'     => 'dummy',
                'lname'     => 'name',
                'amount'    => 42,
                'full-name' => 'dummy name',
            ],
            [
                'fname'     => 'simple',
                'lname'     => 'name',
                'amount'    => 87,
                'full-name' => 'simple name',
            ]
        ];


        return [
            [$input, $expected]
        ];
    }

    public function groupingConcatenating()
    {
        $input = [
            [
                'name'   => 'dummy name',
                'city'   => 'cracow',
                'amount' => 42
            ],
            [
                'name'   => 'another name',
                'city'   => 'cracow',
                'amount' => 20
            ],
            [
                'name'   => 'simple name',
                'city'   => 'warsaw',
                'amount' => 87
            ]
        ];

        $expected = [
            'cracow' => 2,
            'warsaw' => 1
        ];

        return [
            [$input, $expected]
        ];
    }
}
