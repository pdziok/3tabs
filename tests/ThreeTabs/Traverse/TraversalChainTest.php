<?php


namespace ThreeTabs\Traverse;


class TraversalChainTest extends \PHPUnit_Framework_TestCase
{


    public function testChainTraversing()
    {
        $traversalMock = $this->getMock('\\ThreeTabs\\Traverse\\TraversalInterface');
        $traversalMock->expects($this->exactly(1))
            ->method('traverse');

        $anotherTraversalMock = $this->getMock('\\ThreeTabs\\Traverse\\TraversalInterface');
        $anotherTraversalMock->expects($this->exactly(1))
            ->method('traverse');


        $traversal = new TraversalChain(
            [$traversalMock, $anotherTraversalMock]
        );

        $traversal->traverse(
            [
                'first',
                'second'
            ]
        );
    }
}
