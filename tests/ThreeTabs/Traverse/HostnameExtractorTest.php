<?php


namespace ThreeTabs\Traverse;


class HostnameExtractorTest extends \PHPUnit_Framework_TestCase
{


    /**
     * @expectedException \InvalidArgumentException
     */
    public function testTraversingNonExistingField()
    {
        $traversal = new HostnameExtractor('non existing field');
        $traversal->traverse(
            [
                ['some key' => 'dummy value'],
                ['another key' => 'dummy value']
            ]
        );
    }

    /**
     * @expectedException \InvalidArgumentException
     * @dataProvider nonUrlFields
     */
    public function testTraversingNonUrlFields($data)
    {
        $traversal = new HostnameExtractor('some-field');
        $traversal->traverse($data);
    }

    /**
     * @dataProvider urlFields
     */
    public function testTraversingUrlFields($data, $expected)
    {
        $traversal = new HostnameExtractor('url');
        $output    = $traversal->traverse($data);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);
    }

    public function nonUrlFields()
    {
        $data = [
            ['some-field' => md5('onet.pl')],
            ['some-field' => base64_encode('wp.pl')],
            ['some-field' => 'definitely not an url']
        ];

        return [$data];
    }

    public function urlFields()
    {
        $input = [
            ['url' => 'http://onet.pl'],
            ['url' => 'https://bitbucket.org/pdziok'],
            ['url' => 'http://wp.pl/article-seo-name.html?utm_source=Dummy%20source'],
        ];

        $expected = [
            ['url' => 'onet.pl'],
            ['url' => 'bitbucket.org'],
            ['url' => 'wp.pl'],
        ];

        return [
            [$input, $expected]
        ];
    }
}
