<?php


namespace ThreeTabs\Traverse;


class CountTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCountingByNonExistingField()
    {
        $traversal = new Count('non existing');
        $traversal->traverse(
            [
                ['some value', 'another value'],
                ['some value', 'another value']
            ]
        );
    }

    /**
     * @dataProvider simpleCounting
     */
    public function testCountingSimple($input, $expected)
    {
        $traversal = new Count();
        $output    = $traversal->traverse($input);

        $this->assertInternalType('numeric', $output);
        $this->assertEquals($expected, $output);
    }

    /**
     * @dataProvider simpleCounting
     * @expectedException \InvalidArgumentException
     */
    public function testGroupingCountingWithNonExistingGroupingKey($input)
    {
        $traversal = new Count('city');
        $traversal->traverse($input);
    }

    /**
     * @dataProvider groupingCounting
     */
    public function testCountingWithGrouping($input, $expected)
    {
        $traversal = new Count('city');
        $output    = $traversal->traverse($input);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);
    }

    public function simpleCounting()
    {
        $input = [
            [
                'name'   => 'dummy name',
                'amount' => 42
            ],
            [
                'name'   => 'simple name',
                'amount' => 87
            ]
        ];

        $expected = 2;

        return [
            [$input, $expected]
        ];
    }

    public function groupingCounting()
    {
        $input = [
            [
                'name'   => 'dummy name',
                'city'   => 'cracow',
                'amount' => 42
            ],
            [
                'name'   => 'another name',
                'city'   => 'cracow',
                'amount' => 20
            ],
            [
                'name'   => 'simple name',
                'city'   => 'warsaw',
                'amount' => 87
            ]
        ];

        $expected = [
            'cracow' => 2,
            'warsaw' => 1
        ];

        return [
            [$input, $expected]
        ];
    }
}
