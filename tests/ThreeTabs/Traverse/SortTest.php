<?php


namespace ThreeTabs\Traverse;


class SortTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testSortingNotAnArray()
    {
        $traversal = new Sort(Sort::ASCENDING);

        $traversal->traverse('string');
    }

    /**
     * @dataProvider simpleSorting
     */
    public function testSimpleSorting($input, $expected)
    {
        $traversal = new Sort(Sort::ASCENDING);

        $output = $traversal->traverse($input);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);

    }

    /**
     * @dataProvider sortingByField
     * @expectedException \InvalidArgumentException
     */
    public function testSortingByNonExistingField($input, $expected)
    {
        $traversal = new Sort(Sort::ASCENDING, 'value');

        $output = $traversal->traverse($input);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);

    }

    /**
     * @dataProvider sortingByField
     */
    public function testSortingByField($input, $expected)
    {
        $traversal = new Sort(Sort::DESCENDING, 'val');

        $output = $traversal->traverse($input);

        $this->assertInternalType('array', $output);
        $this->assertEquals($expected, $output);

    }

    public function simpleSorting()
    {
        $input = [
            3, 6, 2, 1
        ];

        $expected = [
            3 => 1,
            2 => 2,
            0 => 3,
            1 => 6
        ];

        return [
            [$input, $expected]
        ];
    }

    public function sortingByField()
    {
        $input = [
            [
                'key' => 1,
                'val' => 2
            ],
            [
                'key' => 2,
                'val' => 8
            ],
            [
                'key' => 3,
                'val' => 2
            ],
            [
                'key' => 4,
                'val' => 14
            ],
        ];

        $expected = [
            3 => [
                'key' => 4,
                'val' => 14
            ],
            1 => [
                'key' => 2,
                'val' => 8
            ],
            0 => [
                'key' => 1,
                'val' => 2
            ],
            2 => [
                'key' => 3,
                'val' => 2
            ],
        ];

        return [
            [$input, $expected]
        ];
    }
}
