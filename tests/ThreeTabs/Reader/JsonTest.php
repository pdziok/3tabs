<?php


namespace ThreeTabs\Reader;


class JsonTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider expectedData
     */
    public function testReadingExistingFile($expected)
    {
        $reader = new Json();
        $output = $reader->read(TEST_PATH . '/_files/data.json');

        $this->assertEquals($expected, $output);
    }

    public function expectedData()
    {
        $expected = [
            'name'              => 'pdziok/3tabs',
            'description'       => '...',
            'minimum-stability' => 'stable',
            'license'           => 'proprietary',
            'authors'           => [
                ['name' => 'Paweł Dziok']
            ]
        ];

        return [
            [$expected]
        ];
    }
}
