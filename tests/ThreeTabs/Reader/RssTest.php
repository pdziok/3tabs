<?php


namespace ThreeTabs\Reader;


class RssTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider expectedData
     */
    public function testReadingExistingFile($expected)
    {
        $reader = new Rss();
        $output = $reader->read(TEST_PATH . '/_files/rss.xml');

        $this->assertEquals($expected, $output);
    }

    public function expectedData()
    {
        $expected = [
            [
                'title'       => [],
                'link'        => 'http://www.tvn24.pl/wiadomosci-ze-swiata,2/zabicie-osamy-bin-ladena-atak-na-abbottabad-dzieki-pakistanczykom,515069.html',
                'description' => [],
                'pubDate'     => 'Fri, 13 Feb 15 17:15:00 +0100',
                'guid'        => 'http://www.tvn24.pl/wiadomosci-ze-swiata,2/zabicie-osamy-bin-ladena-atak-na-abbottabad-dzieki-pakistanczykom,515069.html',
            ], [
                'title'       => [],
                'link'        => 'http://www.tvn24.pl/wiadomosci-z-kraju,3/bedzie-sledztwo-ws-naciskow-podczas-sprawy-zabojstwa-zietary,515068.html',
                'description' => [],
                'pubDate'     => 'Fri, 13 Feb 15 17:10:07 +0100',
                'guid'        => 'http://www.tvn24.pl/wiadomosci-z-kraju,3/bedzie-sledztwo-ws-naciskow-podczas-sprawy-zabojstwa-zietary,515068.html',
            ], [
                'title'       => [],
                'link'        => 'http://www.tvnwarszawa.pl/informacje,news,sprawa-gruntu-pod-zlote-tarasy-samorzadowcy-beda-znow-sadzeni,157878.html',
                'description' => [],
                'pubDate'     => 'Fri, 13 Feb 15 16:51:46 +0100',
                'guid'        => 'http://www.tvnwarszawa.pl/informacje,news,sprawa-gruntu-pod-zlote-tarasy-samorzadowcy-beda-znow-sadzeni,157878.html',
            ],
        ];


        return [
            [$expected]
        ];
    }
}
