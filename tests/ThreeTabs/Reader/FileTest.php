<?php


namespace ThreeTabs\Reader;


class FileTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testReadingNonExistingFile()
    {
        $reader = new File();
        $reader->read(TEST_PATH . '/_files/non-existing');
    }

    /**
     * @dataProvider appLog
     */
    public function testReadingExistingFile($expected)
    {
        $reader = new File();
        $output = $reader->read(TEST_PATH . '/_files/app.log');

        $this->assertEquals($expected, $output);
    }

    /**
     * @dataProvider appLogExtracted
     */
    public function testReadingWithExtractingExistingFile($expected)
    {
        $reader = new File('^\[(?P<time>\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\] (.+?)$');
        $output = $reader->read(TEST_PATH . '/_files/app.log');

        $this->assertEquals($expected, $output);
    }

    public function appLog()
    {
        $expected = [
            '[2015-02-11 18:31:15] myapp.INFO: Matched route "GET_" (parameters: "_controller": "{}", "_route": "GET_") [] []',
            '[2015-02-11 18:31:15] myapp.INFO: > GET / [] []',
            '[2015-02-11 18:43:16] myapp.INFO: Matched route "GET_" (parameters: "_controller": "{}", "_route": "GET_") [] []',
            '[2015-02-11 18:43:16] myapp.INFO: > GET / [] []',
        ];

        return [
            [$expected]
        ];
    }

    public function appLogExtracted()
    {
        $expected = [
            ['time' => '2015-02-11 18:31:15'],
            ['time' => '2015-02-11 18:31:15'],
            ['time' => '2015-02-11 18:43:16'],
            ['time' => '2015-02-11 18:43:16'],
        ];

        return [
            [$expected]
        ];
    }
}
