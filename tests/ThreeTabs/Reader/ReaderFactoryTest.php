<?php


namespace ThreeTabs\Reader;


class ReaderFactoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \ThreeTabs\Reader\ReaderFactoryException
     */
    public function testCreatingReadingUsingInvalidType()
    {
        $factory = new ReaderFactory();
        $factory->create([]);
    }

    /**
     * @expectedException \ThreeTabs\Reader\ReaderFactoryException
     */
    public function testCreatingNonConfiguredReader()
    {
        $factory = new ReaderFactory();
        $factory->create('mind');
    }

    public function testCreatingReaderWithoutArguments()
    {
        $factory = new ReaderFactory(
            [
                'file' => File::class
            ]
        );
        $factory->create('file');
    }

    public function testCreatingReaderWithArguments()
    {
        $factory = new ReaderFactory(
            [
                'file' => File::class
            ]
        );
        $factory->create(
            'file',
            [
                'url' => TEST_PATH . '/_files/app.log'
            ]
        );
    }

    public function testCreatingReaderFromConfig()
    {
        $factory = new ReaderFactory(
            [
                'file' => File::class
            ]
        );
        $factory->createFromConfig(
            [
                'type'      => 'file',
                'arguments' => [
                    'url' => TEST_PATH . '/_files/app.log'
                ]
            ]
        );
    }
}
