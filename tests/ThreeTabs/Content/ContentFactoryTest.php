<?php


namespace ThreeTabs\Content;


use ThreeTabs\Reader\ReaderFactoryException;
use ThreeTabs\Traverse\TraversalFactoryException;

class ContentFactoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreatingContentWithoutName()
    {
        $factory = new ContentFactory($this->createReaderFactoryMock(), $this->createTraversalFactoryMock());
        $factory->create([]);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreatingContentWithoutPartial()
    {
        $factory = new ContentFactory($this->createReaderFactoryMock(), $this->createTraversalFactoryMock());
        $factory->create(
            [
                'name' => 'dummy',
            ]
        );
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreatingContentWithoutSections()
    {
        $factory = new ContentFactory($this->createReaderFactoryMock(), $this->createTraversalFactoryMock());
        $factory->create(
            [
                'name'    => 'dummy',
                'partial' => 'main'
            ]
        );
    }

    /**
     * @expectedException \ThreeTabs\Content\ContentFactoryException
     */
    public function testCreatingContentWithoutReaderConfig()
    {
        $factory = new ContentFactory($this->createReaderFactoryMock(), $this->createTraversalFactoryMock());
        $factory->create(
            [
                'name'     => 'dummy',
                'partial'  => 'main',
                'sections' => ['some sections']
            ]
        );
    }

    /**
     * @expectedException \ThreeTabs\Content\ContentFactoryException
     */
    public function testCreatingContentUsingInvalidReaderConfig()
    {
        $readerFactory = $this->createReaderFactoryMock();
        $factory       = new ContentFactory($readerFactory, $this->createTraversalFactoryMock());
        $readerFactory->expects($this->any())
            ->method('createFromConfig')
            ->willThrowException(new ReaderFactoryException());
        $factory->create(
            [
                'name'     => 'dummy',
                'partial'  => 'main',
                'reader'   => 'mock',
                'sections' => ['some sections']
            ]
        );
    }

    /**
     * @expectedException \ThreeTabs\Content\ContentFactoryException
     */
    public function testCreatingContentWithoutTraversalConfig()
    {
        $readerFactory    = $this->createReaderFactoryMock();
        $traversalFactory = $this->createTraversalFactoryMock();
        $factory          = new ContentFactory($readerFactory, $traversalFactory);

        $reader = $this->createFileReaderMock();

        $readerFactory->expects($this->any())
            ->method('createFromConfig')
            ->willReturn($reader);

        $factory->create(
            [
                'name'     => 'dummy',
                'partial'  => 'main',
                'reader'   => [
                    'url' => 'mock'
                ],
                'sections' => [
                    'name' => 'some section'
                ]
            ]
        );
    }

    /**
     * @expectedException \ThreeTabs\Content\ContentFactoryException
     */
    public function testCreatingContentWithInvalidTraversalConfig()
    {
        $readerFactory    = $this->createReaderFactoryMock();
        $traversalFactory = $this->createTraversalFactoryMock();
        $factory          = new ContentFactory($readerFactory, $traversalFactory);

        $reader = $this->createFileReaderMock();

        $readerFactory->expects($this->any())
            ->method('createFromConfig')
            ->willReturn($reader);

        $traversalFactory->expects($this->any())
            ->method('createFromConfig')
            ->willThrowException(new TraversalFactoryException());

        $factory->create(
            [
                'name'     => 'dummy',
                'partial'  => 'main',
                'reader'   => [
                    'url' => 'mock'
                ],
                'sections' => [
                    [
                        'name'      => 'some section',
                        'traversal' => 'invalid'
                    ]
                ]
            ]
        );
    }

    public function testCreatingContent()
    {
        $readerFactory    = $this->createReaderFactoryMock();
        $traversalFactory = $this->createTraversalFactoryMock();
        $factory          = new ContentFactory($readerFactory, $traversalFactory);

        $reader = $this->createFileReaderMock();

        $readerFactory->expects($this->any())
            ->method('createFromConfig')
            ->willReturn($reader);

        $sort = $this->getSortTraversalMock();

        $traversalFactory->expects($this->any())
            ->method('createFromConfig')
            ->willReturn($sort);

        $sort->expects($this->any())
            ->method('traverse')
            ->willReturn(['some dummy data']);

        $factory->create(
            [
                'name'     => 'dummy',
                'partial'  => 'main',
                'reader'   => [
                    'url' => 'mock'
                ],
                'sections' => [
                    [
                        'name'      => 'some section',
                        'traversal' => ['mock']
                    ]
                ]
            ]
        );
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function createReaderFactoryMock()
    {
        return $this->getMock('ThreeTabs\\Reader\\ReaderFactory');
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function createTraversalFactoryMock()
    {
        return $this->getMock('ThreeTabs\\Traverse\\TraversalFactory');
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function createFileReaderMock()
    {
        return $this->getMockBuilder('ThreeTabs\\Reader\\File')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    private function getSortTraversalMock()
    {
        return $this->getMockBuilder('ThreeTabs\\Traverse\\Limit')
            ->disableOriginalConstructor()
            ->getMock();
    }
}
