<?php


namespace ThreeTabs\Content;

class ContainerBuilderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testBuildingContainerWithEmptyConfig()
    {
        $contentFactory = $this->getContentFactoryMock();
        $builder        = new ContainerBuilder($contentFactory);

        $builder->build([]);
    }

    public function testBuildingContainerWithTwoElements()
    {
        $contentFactory = $this->getContentFactoryMock();
        $builder        = new ContainerBuilder($contentFactory);

        $contentFactory->expects($this->any())
            ->method('create')
            ->willReturn($this->getElementMock());

        $builderConfig = ['some element', 'another element'];
        $output        = $builder->build($builderConfig);

        $this->assertInstanceOf(Container::class, $output);
        $this->assertCount(count($builderConfig), $output->getIterator()->getArrayCopy());
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getContentFactoryMock()
    {
        return $this->getMockBuilder('ThreeTabs\\Content\\ContentFactory')
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getElementMock()
    {
        return $this->getMockBuilder('ThreeTabs\\Content\\Element')
            ->disableOriginalConstructor()
            ->getMock();
    }
}
