<?php


namespace ThreeTabs\Content;


class ElementTest extends \PHPUnit_Framework_TestCase
{


    public function testSettingConstructor()
    {
        $partial = new \stdClass();
        $element = new Element('dummy name', ['some data'], $partial);
        $this->assertEquals('dummy name', $element->name);
        $this->assertEquals(['some data'], $element->data);
        $this->assertEquals($partial, $element->partial);
    }
}
