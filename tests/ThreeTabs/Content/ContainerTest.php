<?php


namespace ThreeTabs\Content;


class ContainerTest extends \PHPUnit_Framework_TestCase
{


    public function testIteratorHasSameElementsAsProvidedInConstructor()
    {
        $elements = [
            $this->getElementMock(),
            $this->getElementMock(),
            $this->getElementMock(),
        ];

        $container = new Container($elements);
        $iterator  = $container->getIterator();
        while ($iterator->valid()) {
            $this->assertArrayHasKey($iterator->key(), $elements);
            $this->assertSame($elements[$iterator->key()], $iterator->current());
            $iterator->next();
        }
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getElementMock()
    {
        return $this->getMockBuilder('ThreeTabs\\Content\\Element')
            ->disableOriginalConstructor()
            ->getMock();
    }
}
